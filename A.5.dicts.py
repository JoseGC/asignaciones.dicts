# Read a string:
# s = input()
# Print a value:
# print(s)
N = int(input())
palabras = {}
for i in range(N):
    lista = list(input().split())
    palabras[lista[0]] = lista[1:len(lista)]
M = int(input())
for t in range(M):
    a = list(input().split())
    if 'W' in palabras[a[1]] and a[0] == 'write':
        print('OK')
    elif 'R' in palabras[a[1]] and a[0] == 'read':
        print('OK')
    elif 'X' in palabras[a[1]] and a[0] == 'execute':
        print('OK')
    else:
        print('Access denied')