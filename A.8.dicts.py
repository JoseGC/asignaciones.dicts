# Read a string:
# s = input()
# Print a value:
# print(s)
dicionario = {}
for _ in range(int(input())):
  ingre, *latin = input().replace('- ', '').replace(',', '').split()
  for palabra in latin:
    if palabra not in dicionario.keys():
      dicionario[palabra] = []
    dicionario[palabra].append(ingre)

print()
print(len(dicionario))
for i in sorted(dicionario.keys()):
  print(i, '-', ', '.join(sorted(list(dicionario[i]))))